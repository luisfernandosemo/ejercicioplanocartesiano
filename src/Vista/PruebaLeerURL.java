/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import ufps.util.varios.*;

/**
 *
 * @author madar
 */
public class PruebaLeerURL {
    
    public static void main(String[] args) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/coordenascartesianas/-/raw/master/puntos.csv");
        //ArchivoLeerURL archivo=new ArchivoLeerURL("http://www.madarme.co");
        Object datos[]=archivo.leerArchivo();
        for(Object filas:datos)
            System.out.println(filas.toString());
    }
    
}
