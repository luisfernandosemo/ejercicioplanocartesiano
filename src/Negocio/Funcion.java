/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Punto;

/**
 * Clase que representa una colección de puntos de una función
 * @author madar
 */
public class Funcion {
    
    private Punto puntos[];

    public Funcion() {
    }

    public Punto[] getPuntos() {
        return puntos;
    }

    public void setPuntos(Punto[] puntos) {
        this.puntos = puntos;
    }
    
    
    
}
